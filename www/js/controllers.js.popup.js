/**
 * このファイルはライセンスフリーです。ご自身の責任の下、ご自由にお使いください。
 */

angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope) {})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope, $ionicPopup) {
  $scope.settings = {
    enableFriends: true
  };
  
  $scope.popupText = 'サンプル'; // ポップアップに表示する初期状態のテキスト
  $scope.countDown = 5; // ポップアップを閉じるために必要なタップ数
  $scope.buttonCountUp = 0; // ボタンを押した回数を記録する
  
  $scope.showPopup = function() {
    $scope.mypop = $ionicPopup.show({
        // 通常の方法では閉じれないようにする代わりに、Xボタンを押したら閉じるように`close()`を呼んでいます
        template: '<ion-header-bar align-title="center" class="bar-positive"><h1 class="title">ポップアップ</h1>\
          <button class="button" ng-click="mypop.close()"><i class="icon ion-close-round"></i></button></ion-header-bar><p>{{popupText}}</p>',
      scope: $scope,
      buttons: [{
        text: 'Cancel-1', // ボタンに表示する文字列です
        type: 'button-default', // ボタンの見た目を決定づけるクラスです
        onTap: function(e) {
          e.preventDefault(); // ボタンを押しても閉じれないようにしている
          $scope.popupText = 'このポップアップは閉じることができません'; // ポップアップ上に表示している文字を変更している
          $scope.buttonCountUp++;
        }
      }, { 
        text: 'Cancel-2',
        type: 'button-assertive', 
        onTap: function(e) {
          e.preventDefault();
          $scope.popupText = '残念！閉じれません！';
          $scope.buttonCountUp++;
        }
      }, {
        text: 'OK',
        type: 'button-positive',
        onTap: function(e) {
          $scope.countDown--; 
          if($scope.countDown > 0) {
            e.preventDefault();
            $scope.popupText = "あと" + $scope.countDown + "回で閉じれます";
          } else {
            $scope.countDown = 5;
            $scope.popupText = 'サンプル';
            console.log('おつかれさまでした');
          }
          $scope.buttonCountUp++;
        }
      }]
    });
  }
});
