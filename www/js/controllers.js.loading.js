/**
 * このファイルはライセンスフリーです。ご自身の責任の下、ご自由にお使いください。
 */

angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope) {})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats, $ionicLoading, $timeout) {
  $scope.chat = Chats.get($stateParams.chatId);

  const waitTime = 2000; // ページ読み込み後、ローディングアイコンを表示するまでの時間(ミリ秒)
  const loadingTime = 5000; // ローディングアイコン表示後、ローディングアイコンを非表示にするまでの時間(ミリ秒)

  console.log('ローディングアイコンの読み込みを開始します');
  $ionicLoading.show({
    template: '<ion-spinner icon="circles" class="spinner-positive"></ion-spinner>', // ローディングアイコンを指定しています
    noBackdrop: true, // 画面全体を覆う半透明の背景を非表示にします
    delay: waitTime　// 読み込みから2秒後にローディングアイコンを表示します
    // $timeout関数を消して、ここに次のように書いても実質的に同じ duration: loadingTime
  });

  $timeout(function() { // AngularJSにて用意されているタイムアウト用の関数
    console.log('ローディングアイコンの読み込みを終了します');
    $ionicLoading.hide(); // 表示中のローディングを隠します
  }, waitTime + loadingTime); // ローディングアイコン表示後から5秒後にローディングアイコンを非表示にします
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
